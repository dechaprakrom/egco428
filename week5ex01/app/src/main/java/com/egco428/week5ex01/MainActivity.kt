package com.egco428.week5ex01

import android.app.ListActivity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter

class MainActivity : ListActivity() {

    var mobileList = arrayOf(  "Samsung","Apple","Microsoft","Cisco","Alphabet","HP")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)

        //จะเอา array ไปวางต้องใช้ adapter
        val adapter = ArrayAdapter(this , android.R.layout.simple_list_item_1 , mobileList)
        listAdapter = adapter
        //listAdapter ต้องมี class MainActivity:ListActivity()
        //เพื่อบอกว่าใช้ที่ไหน (this) , style ไหน(android.R.layout.simple_list_item_1) , จากตัวแปรไหน(mobileList)
    }
}
