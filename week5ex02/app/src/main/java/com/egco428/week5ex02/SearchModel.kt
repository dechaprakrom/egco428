package com.egco428.week5ex02

import ir.mirrajabi.searchdialog.core.Searchable
/**
 * Created by pongdec on 9/21/17.
 */
class SearchModel (private var mTitle:String):Searchable{
    override  fun getTitle():String{
        return mTitle
    }
}