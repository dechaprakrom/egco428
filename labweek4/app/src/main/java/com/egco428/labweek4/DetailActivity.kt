package com.egco428.labweek4

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

    val TAG = "DetailActivity"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        Log.d(TAG, "onCreate")
        val bundle = intent.extras
        var inp1: String = ""
        var inp2: String = ""

        if (bundle != null){
            inp1 = bundle.getString("input1")
//            user2.setText("$inp1")
            inp2 = bundle.getString("input2")
//            pass2.setText("$inp2")
            //รับค่า input1 จาก MainActivity
        }

        val myHashMap = hashMapOf<String,String>("username" to "a" ,"password" to "b")
        if(myHashMap["username"].toString() == inp1 && myHashMap["password"].toString() == inp2){
//           Log.d("test","ok")
            user2.setText("Username "+ "${inp1}")
            pass2.setText("Password "+ "${inp2}")
        }else{

            var intent = Intent(this,MainActivity::class.java)
            startActivity(intent)
//            this.finish()
        }

        backBtn.setOnClickListener {
            var intent = Intent(this,MainActivity::class.java)
            startActivity(intent)
        }


    }

}
