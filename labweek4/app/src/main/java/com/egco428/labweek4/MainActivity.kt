package com.egco428.labweek4

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val TAG = "MainActivity"
    private var photoImage: ImageView? = null
    private var flag = true



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d(TAG,"onCreate")


        submitBtn.setOnClickListener {
            var intent = Intent(this,DetailActivity::class.java)

            intent.putExtra("input1",userText.text.toString())
            intent.putExtra("input2",passText.text.toString())
            startActivity(intent)
        }

        cancelBtn.setOnClickListener{
            userText.text = null
            passText.text = null
        }
    }


}
