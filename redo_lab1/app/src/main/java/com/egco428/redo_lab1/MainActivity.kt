package com.egco428.redo_lab1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        submitBtnMain.setOnClickListener {
            var intent = Intent(this,DetailActivity::class.java)

            intent.putExtra("messageMain1",inputMain1.text.toString())
            intent.putExtra("messageMain2",inputMain2.text.toString())
            startActivity(intent)

        }

        cancelBtnMain.setOnClickListener {
            inputMain1.text = null
            inputMain2.text = null
        }


    }

}
