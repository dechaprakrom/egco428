package com.egco428.redo_lab1

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        var bundle = intent.extras
        var input1:String = ""
        var input2:String = ""

        if (bundle!=null){
            input1=bundle.getString("messageMain1")
            input2=bundle.getString("messageMain2")

            labelDetail1.text = input1
            labelDetail2.text = input2
        }


        plusBtnDetail.setOnClickListener {

            labelDetail3.text = "${input1}+${input2}"
        }

        backBtnDetail.setOnClickListener {
            var intent = Intent(this,MainActivity::class.java)
            startActivity(intent)
        }


    }
}
