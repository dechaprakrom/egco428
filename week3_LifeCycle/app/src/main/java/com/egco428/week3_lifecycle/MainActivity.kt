package com.egco428.week3_lifecycle

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*
import javax.security.auth.Destroyable

class MainActivity : AppCompatActivity() {

    val TAG = "MainActivity"
    private var photoImage: ImageView? = null
    private var flag = true



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d(TAG,"onCreate")

        photoImage = findViewById(R.id.imageView)
        photoImage!!.setImageResource(R.drawable.trekkie)

        nextBtn.setOnClickListener {
            var intent = Intent(this,DetailActivity::class.java)

            intent.putExtra("input1",inputText1.text.toString())
            startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main,menu) //ผูก object


        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        if(id == R.id.action_home){
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun actionResourceClickHandler(item: MenuItem){
        var intent = Intent(this,DetailActivity::class.java)

        intent.putExtra("input1",inputText1.text.toString())
        startActivity(intent)
        finish()
    }

    fun actionImageChangeHandler(item: MenuItem?){
        photoImage = findViewById(R.id.imageView)
        if(flag){
            photoImage!!.setImageResource(R.drawable.wilson)
        }else{
            photoImage!!.setImageResource(R.drawable.trekkie)
        }
        flag = !flag
        //สลับ on off ไปเรื่อยๆ
    }





    override fun onRestart(){
        super.onRestart()
        Log.d(TAG,"onRestart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG,"onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG,"onPause")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG,"onDestroy")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG,"onStop")
    }
    override fun onStart() {
        super.onStart()
        Log.d(TAG,"onStart")
    }
}
